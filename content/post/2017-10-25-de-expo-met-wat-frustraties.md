---
title: DE EXPO met wat frustraties
date: 2017-10-25
---
We waren al vroeg op school, alles was netjes neergezet voor de Expo. Na een vage start was blijkbaar de Expo begonnen. 
Na een tijdje kregen we veel studenten die kwamen kijken bij ons project. Iedereen was er enorm enthousiast over en gaven ons goede feedback. 
De vragen die werden gesteld konden we goed beantwoorden. We kregen maar liefst 26 stickers. 
De communicatie onder de docenten was niet helemaal optimaal, want er werd nog gestickerd door de docenten maar de puntentelling was al langs geweest. 
Ik vroeg mij wel af waar onze docent was, want ze zouden bij iedereen even een kijkje nemen. We hadden als groepje helaas niet gewonnen, 
maar onze samenwerking was wel top geweest. 


Door nog even na te vragen aan Meerte hoe ze het graag wou dat het ingeleverd werd, werd ik lichtelijk gefrustreerd.
Hier werd ineens verteld dat je alles moest linken naar je blog, gelukkig heb ik die wel altijd up-to-date gehouden. 
Wat een week geleden nog was dat we een extra bijlage moesten inleveren met daarin het bewijsmateriaal was dus nu ineens linken. 
Ik heb natuurlijk alles al ready en vindt het gewoon vervelend dat er elke keer wat anders gezegd word. 
Want eerste werd er verteld dat het blog altijd jouw eigen plekje was persoonlijk om daar je ontwikkeling in te plaatsen. 
Ons werd toen uitgelegd dat je daar je daar opschreef wat je geleerd had en globaal wat je had gedaan. 
Ik krijg steeds meer het gevoel dat de docenten zelf niet weten wat ze moeten doen. En wij betalen wel allemaal enorm veel geld om een studie te volgen. 
Daardoor heb ik geen idee wat ik nou allemaal aan het doen ben. Het frustreert mij dan dat iteratie 2 nog niet eens naar gekeken is wat er is ingeleverd en wij wel alles op tijd af moeten hebben.


Na de expo werden we teruggeroepen om nog even wat duidelijkheid te geven over de STARRTS. 
Daarin moesten we ook opnieuw de peerfeedback invullen van je teamgenoten. Ik was erg blij met mijn resultaten, ik kreeg allemaal plusjes.
Ook zag ik dat er vooruitgang was geboekt in mijn ontwikkeling en dat mijn medestudenten dat ook ervaren hebben. 
 <img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/eindexpo.jpg" alt="expo">
