---
title: Theorie in praktijk toepassen, COCD BOX & ontwerpproceskaart 1e deel
date: 2017-09-26
---
In de ochtend ben ik samen met Jari Luiendijk en Ricardio het eerste deel (iteratie 1.) van de ontwerpproceskaart gaan maken. 
Iedereen knipte een paar blaadjes uit. Sommige papiertjes lagen niet helemaal goed op zijn plek. 
Ik gaf hierbij een uitleg dat het misschien anders geplakt moest worden omdat sommige onderdelen bij elkaar horen. 
Met wat samenwerking en geschuif kwam het ontwerpproceskaart tot zijn recht. 


Door de Theorieën die we de afgelopen weken gehad hebben, merk ik dat het mij lukt om veel breder te denken. 
Doormiddel van de post-it’s met ideeën ga je divergeren. Vanuit daar ga je een betere selectie maken wat voor de 
hand liggend is en was een uniek idee zou kunnen worden. Wat N.V.T is qua idee ga je weer een oplossing zoeken zodat 
het wel zou kunnen werken. Vervolgens convergeer je het weer en ga je het opnieuw in hokjes plaatsen doormiddel van de COCD box. 
Wat is een NOW, HOW of juist WOW!. Na een lange brainstorm en na veel divergeren en convergeren kom je uit tot een paar overgebleven post-it’s. 
Dit zijn de post-it’s waarmee we mee aan de slag zijn gegaan. Door dit te doorlopen is ons nieuwe concept ontstaan voor het Rotterdamse spel.  
  <img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/proceskaart.jpg" alt="proceskaart">
 <img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/analyse.jpg" alt="analyse">