---
title: Iteratie 2 presenteren
date: 2017-10-04
---
Uit handen nemen? Ja, dat vind ik enorm moeilijk. Liefst zou ik alles willen doen maar het is een team opdracht. 
Deze keer presenteerde ik niet ons spel, ik keek ernaar en observeerde de presentatie alsof ik iemand 
was die niet van het spel afwist. Leerpuntje voor de volgende keer voor mijzelf en de groep: 
Dat we volgende keer de presentatie met ze alle nog eens doornemen. Dan zie je wat wel relevant is om te vertellen en wat niet.
