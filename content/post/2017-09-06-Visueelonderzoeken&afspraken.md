---
title: Visueel onderzoeken & afspraken
date: 2017-09-06
---
Opnieuw ben ik de stad weer in gegaan om beeldmateriaal te schieten voor het thema “Constructies in Rotterdam”. 
Doormiddel van close-ups van constructies, materialen en gebouwen in Rotterdam kreeg ik nieuwe inspiratie. 
Ik zorgde voor een goede weergaven van constructies in Rotterdam doormiddel van een collage. 
De strakke indeling van de collage zorgde voor duidelijk en overzichtelijkheid.
	
In mijn moodboard verbeelde ik mijn eik persoon van een bouwkundestudent.
Wat doet een bouwkunde student in zijn vrije tijd? Waar gaat hij uiteten? Speelt hij/zij graag games? 
Door kleine vragen te stellen aan jezelf, kreeg ik de gewenste moodboard. Nadat iedereen de moodboard en collage afgerond hadden, 
lieten we deze aan elkaar zien. Zo om feedback op elkaar te geven, wat goed en wat beter zou kunnen. 

Als teamcoach vroeg ik alle aandacht voor het maken van de teamafspraken en de gezamenlijke doelen. 
Hierbij kwam je erachter wat voor mensen er in je team zit, wat je aan elkaar hebt en wat zijn of haar doelen zijn. 
Door dit op te stellen kun je ook beter rekening houden met elkaar. Ik stelde het op en betrok iedereen uit het team om regels te verzinnen.
 <img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/bespreking.jpg" alt="bespreking">

