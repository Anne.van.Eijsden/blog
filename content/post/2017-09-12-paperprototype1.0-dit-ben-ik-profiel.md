---
title: Paperprototype 1.0 & "Dit ben ik profiel"
date: 2017-09-12
---
In het weekend had ik al wat schetsjes gemaakt hoe we het prototype zouden kunnen maken. Het belangrijkste is eenvoud, 
duidelijkheid en gebruiksvriendelijkheid. Hoe maak ik een prototype die goed de beleving van het spel kan weergeven? 
Doormiddel van een karton mobieltje natuurlijk! Waarin we een paar slides kunnen laten zien doormiddel van A4 papier. 
Omdat je de plattegrond moet openspelen heb ik bedacht om daar schuifstaafjes van te maken, van zwart silhouet naar kleur. Samen met Ricardio zette ik dit prototype inelkaar.

**"Dit ben ik profiel:"**
“Wie ben ik profiel” is een belangrijk onderdeel voor mijn zelfontwikkeling. 
Door het wie ben ik profiel heb ik goed op kunnen schrijven wat mijn sterke en zwakke punten zijn. 
Uit de test was gebleken dat ik een zorgdrager ben. Dit klopt ook wel, 
ik ben iemand die veel verantwoordelijkheidsgevoel heeft in teams en altijd iedereen wilt helpen. 
Ik weet nu beter wat mijn kansen en bedreigingen zijn door de SWOT-analyse. Na de “Dit ben ik profiel” 
ben ik bewuster geworden omdat je voor jezelf alles op een rijtje zet. De zwakke punten en aandachtspuntjes kan ik weer aan werken.  
<img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/bijlage4-5.pdf" alt="Ditbenikprofiel">
<img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/bijlage6-7.pdf" alt="Ditbenikprofiel">
<img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/paperprototype-1.pdf" alt="Paperprototype">