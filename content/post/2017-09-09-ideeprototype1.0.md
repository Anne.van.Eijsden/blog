---
title: Idee prototype 1.0
date: 2017-09-09
---
Al is het weekend, soms heb je gewoon ineens een idee! 
En dat idee daar moet je wat mee doen. 
Door te onderzoeken naar wat voor elementen we nodig hebben om het prototype 
zo gebruiksvriendelijk mogelijk te maken. 
Kwam ik erachter dat we helemaal niet veel nodig hadden om het goed over te brengen. 
Ik zette in stappen neer hoe het prototype gemaakt zou kunnen worden. 
Zie dummy prototype schetsen

Ook zocht ik naar beelden die ons zouden kunnen inspireren voor de vormgeving. 
Zie beeldend onderzoek app prototype.
 <img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/spelidee-2.jpg" alt="logoschets">
  <img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/onderzoek-online-app.jpg" alt="spelidee">
