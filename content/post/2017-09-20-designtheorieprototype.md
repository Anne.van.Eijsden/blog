---
title: Design Theorie Prototype
date: 2017-09-20
---
Met Design Theorie werkte je met de dubbel diamant. 
Door enorm breed te denken kom je op de gekste dingen, waaruit je een selectie maakt. 
Daarop ga je door, verfijn je alles steeds maar en selecteer je de relevante en interessante ideeën.
Ik heb na deze les gezien dat ik door deze methode enorm breed en origineel kan zijn. 
Dat ik op ideeën kom waarvan ik op het begin niet wist dat ik die kon hebben. Hier ga ik zeker vaker mee werken.
