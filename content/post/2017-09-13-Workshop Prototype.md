---
title: Design Theorie & workshop Prototype
date: 2017-09-13
---
**Design Theorie**
Bij Design Theorie moest je afbeeldingen meenemen een communicatief-retorisch doel hadden. 
Vanuit die afbeeldingen ging je de factoren benoemen. Hierdoor leer je bewuster te worden van de gestalt wetten in afbeeldingen.

**1e Workshop prototype**
Samen met Ricardio heb ik de eerste workshop prototype gevolgd. 
We moesten met een groepje gaan brainstormen om iets te maken van alle rommel uit de bak. 
Door alles op te schrijven wat je denkt creëer je een bredere en sterkere brainstorm. 
Na deze workshop weet ik nu dat je met de meeste onzin spullen een goed prototype kan maken. 
Zolang het maar gebruiksvriendelijk en duidelijk is wat je bedoelt. 