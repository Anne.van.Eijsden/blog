---
title: Paper prototype 2.0 afronden
date: 2017-09-29
---
Omdat wij als groep 4 oktober alles moeten inleveren leek mij het handig om alles vandaag 
af te ronden zodat we dinsdag het spel konden observeren. Samen met andere teamleden hebben 
we het paper prototype afgemaakt. Ik kwam na deze dag erachter dat het maken van een nieuw 
prototype soms langer kan duren dan je in gedachten had. We hadden Myrthe erbij gehaald om feedback gevraagd.

**Feedback van Myrthe:**
**“ Goed idee! Probeer het concept idee nog beter uit te werken zonder dat jullie het spel erin 
uitleggen. Het is een duidelijk leuk spel, ik hoop dat die ook echt uitgewerkt word!”*
 <img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/prototype-2.jpg" alt="prototype2.0">
