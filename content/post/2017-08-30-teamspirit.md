---
title: Team Spirit
date: 2017-08-30
---
Onze eerste opdracht voor ons team was het maken van een huisstijl/teamlogo.
Doormiddel van een brainstorm ben ik op verschillende ideeën gekomen.  
Ik zocht naar de kwaliteiten in een object, zoals bijvoorbeeld de kleur zwart betekende
voor mij de kracht voor ons team. De lichte kleuren voor klantvriendelijkheid. Zo ontstond 
er een krachtig object. Het logo wat ik ontworpen had was geïnspireerd door een video klip
"Coheed and Cambria - You Got Spirit, Kid’ die ik toevallig in het weekend gezien had. 
<img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/spirit-logo-2.png" alt="teamspirit">


