---
title: Spelanalyses 2.0 & beroepsprofiel
date: 2017-09-22
---
Vrijdag, spelletjes dag. Iedereen uit ons team had de taak om een spel te observeren en te analyseren. 
Zelf onderzocht ik het spel rond de tafel. Doormiddel van een goede spel analyse kom je te weten of die een 
spel zou kunnen zijn wat je zou kunnen gebruiken voor jouw doeleinde. 
Aan het einde van de dag kregen we studie coaching. Hierbij kregen we uitleg over het beroepsprofiel. 
Hiermee ben ik gelijk mee aan de slag gegaan. Ik vond het interessant om te weten te komen wat mijn toekomst zou kunnen zijn. 
Ik wist niet dat er zoveel opties waren als je afgestudeerd was als CMD’er. 
 <img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/bijlage8-9.pdf" alt="beroepsprofiel">

