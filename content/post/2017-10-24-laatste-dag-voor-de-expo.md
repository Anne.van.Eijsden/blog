---
title: Laatste dag voor de EXPO
date: 2017-10-24
---
Dit was de laatste dag voor de Expo, als team hebben we nog alle documenten goed nagelopen om het te kunnen printen. 
Ricardio had onze app opgenomen hoe je als gebruiker de App kunt gebruiken. Dit filmpje ben ik gaan monteren in Primere pro. 
Ik heb Ricardio gevraagd om erbij te komen zitten omdat die graag de programma’s wilt leren. Stapje voor stapje legde ik uit hoe ik het filmpje monteerde. 
Ons product wilde we niet op normaal papier laten drukken omdat dit dat te dun werd. We kochten bij de HEMA-fotopapier voor de bouw en bevestigingskaarten. 
Samen met Katja schreven we de pitch. In de tussentijd hadden we ook de laatste ontwerpproceskaart gekregen die we in elkaar moesten zetten. 
Wij hadden dat allang digitaal gedaan en vonden het best wel laat dat dit nu nog gegeven werd.


Omdat ik het keuzevak Sketch had, was ik heel laat thuis. Ik heb tot 00:00 aan school gezeten om alles goed te printen en te knippen. 
Ook heb ik het document van de groep nog ingeleverd op natschool. Deze dag was een productieve dag.

zie filmpje:
https://www.youtube.com/watch?v=ZGCcBB4rndU&feature=youtu.be