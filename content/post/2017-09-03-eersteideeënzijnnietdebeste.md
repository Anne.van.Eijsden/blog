---

title: Eerste ideeën zijn niet de beste
date: 2017-09-03

---
Op weg naar huis in de bus kreeg ik ineens een idee. 
Mijn idee was ineens heel simpel en clear. 
Deze ideeën ben ik gelijk uit gaan schrijven in mijn dummy. 
Maar was dit wel al een goede stap? Sla ik niet allemaal stappen over doordat ik al spellen weet?

Naar aanleiding van mijn spelideeën ben ik onderzoeken gaan doen naar die spellen.
**<a href="https://anne.van.eijsden.gitlab.io/features/pages/spel-idee">Zie onderzoek spel ideeën.</a>** 
   Na het onderzoeken van mijn spel ideeën en mijn QR-idee 
ben ik tot de conclusie dat we moeten afstappen van de QR-codes. Omdat de QR-code 
geen goede verbinding maakt met de studenten heb ik dit ook achterwegen gelaten. 
Zelf vond ik het wel een interessant onderzoek. <img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/spelidee.jpg" alt="spelidee">