---
title: Workshop & feedback Fedde
date: 2017-09-08
---
De workshop “moodboards maken" inspireerde mij voor het maken van mijn collage en moodboard. 
Ik had mijn moodboard en collage al af maar het kon geen kwaad om hier alsnog naar te luisteren. 
Hier kun je altijd nieuwe inspiratie van krijgen of tips. Het werd mij duidelijk dat je beter kunt teruggaan op plakken, 
knippen en dat je moet afstappen van digitaal. Hierdoor zoek je minder specifiek op internet naar het geschikte plaatje 
en kun je alleen de beelden pakken die al in bladen staan. 

<i class="icon-pencil"></i> **FEEDBACK**: 
Na de workshop ben ik feedback gaan vragen aan Fedde. 
Ik liet mijn digitale moodboard en collage zien. 
Ik overtuigde Fedde door te vertellen wat deze eik persoon in zijn dagelijks leven doet. 
Waar hij stapt? Welke games hij speelt? Fedde vond het een goed moodboard. 
Hij gaf mij wel de feedback om de afbeelding van de game controler te vervangen naar een echte game 
zodat het minder breed gericht was. Deze feedback heb ik daarna ook snel verwerkt in mijn moodboard.

De collage vond hij sterkt maar gaf hierbij de feedback mee dat het misschien beter is om minder beeld 
te gebruiken zodat de collage nog sterker wordt. En dat ik misschien kan kijken naar echte materialen 
zoals hout en stof dat ik er misschien bij kan plakken. Ik onderbouwde mijn argument waarom ik gekozen 
had voor meerdere afbeeldingen, waardoor Fedde begreep waarom ik daarvoor had gekozen. 
De feedback van de stofjes daar ga ik zeker mee aan de slag, kijken of dat effect heeft. 

**Wat deed ik ermee:** 
*Less is more, een paar sterke beelden kunnen al genoeg zeggen. 
En stap van digitaal af ga terug naar basic. Ook hoeft feedback 
die je krijgt niet altijd het feedback te zijn toe je moet toepassen. 
Als je goed kan onderbouwen waarom je hiervoor gekozen hebt, is jouw idee bijna nooit fout.*

 <img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/bijlage10-11.pdf" alt="moodboard-collage">
