---
title: Brainstormen,beeldmateriaal & feedback Bob
date: 2017-09-05
---
Ik ben iemand die structuur en duidelijkheid nodig heeft om tot een goed idee te komen. 
Ik betrapte mij zelf erop na het krijgen van de briefing dat ik helemaal verkeerd was begonnen.  
Ik heb wel gebrainstormd naar ideeën en ben wel op onderzoek uit gegaan. 
Maar heb alle belangrijke elementen overgeslagen. Waar is mijn doelgroep? 
Thema? Voor wie wil ik een spel maken? Dus ik deed een paar stappen terug en begon opnieuw.
	
 "Bouwkunde studenten". Waar denk je aan bij bouwkunde studenten? Wat voor beroep?
 Wat leren ze met bouwkunde? Door goed de doelgroep te onderzoeken vormde dit tot een eik persoon. 

Vervolgens kwamen we na het brainstormen uit op het thema. Het thema is "Constructies in Rotterdam". 
Ik ben opnieuw de stad in gegaan om beeldmateriaal te verzamelen voor mijn collage.


<i class="icon-pencil"></i> **FEEDBACK**: 
*Ik heb mijn eerste feedback gevraagd aan Bob naar mijn blog. Ben ik goed bezig? Schrijf ik op de goede manier. 
Ik mocht iets minder schrijven als een dagboek. Deze feedback heb ik met beide handen aangepakt en alleen moet schrijven wat mijn leerfases zijn, 
wat mij opvalt en wat mijn leermomenten zijn. Ik mag wel mijn onderzoeken doorverwijzen in mijn blog. Dat vond ik een goede oplossing.*
 <img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/inspiratie.jpg" alt="inspiratiebeeld">
