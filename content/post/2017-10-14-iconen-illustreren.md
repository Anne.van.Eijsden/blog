---
title: Iconen illustreren
date: 2017-10-14
---
Zelf dacht ik; Als ik nou zo veel mogelijk afrond in het weekend, dan heb ik nog een rustige vakantie. 
Ik begon op mijn zaterdagochtend gelijk aan het schetsen van de iconen. Deze maakte ik iconisch zoals bijvoorbeeld de Markthal> de punteslijper is. 
Zo heb je een klein feitje en achterliggende gedachte bij de plekken in Rotterdam. 
Dit verbindt Rotterdam en de studenten met elkaar, vooral als je nog Rotterdam niet goed kent.
Na het maken van de iconen voor op de kaart, ben ik de map gaan ontwerpen waar de iconen op komen te staan. 
Hierbij maakte ik ook een voorbeeld voor het beginscherm voor Katja. Om te weten of het groepje het goed eruit vond zien heb ik dit in een Mock-up gezet 
en doorgestuurd via WhatsApp. Hier kreeg ik feedback op wat aangepast kon worden of wat juist heel leuk was. 

**Ik kreeg feedback op mijn iconen:**
*Bij het ontwerpen van mijn iconen had ik een schaduw gebruikt om wat beter contrast te geven met de achtergrond. 
Omdat in de huisstijl stond dat er geen schaduw gebruikt werd maar donkere vlakken moest ik dit aanpassen, dit heb ik ook gelijk gedaan. 
Zelf vond ik dit ook beter staan dan een schaduw.*
 <img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/appuitwerken.jpg" alt="uitwerken">
 
