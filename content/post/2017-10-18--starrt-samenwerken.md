---
title: STARRT samenwerken
date: 2017-10-18
---
Vandaag ben ik vrij, geen werk en geen verplichtingen dus dat wordt weer aan school zitten. 
Ik ben gaan starten aan mijn eerste STARRT. Op het begin wist ik niet goed hoe ik moest starten, 
ik zocht de mail erbij met het voorbeeld. Ook pakte ik de competentie lijst erbij waar je precies kunt zien waarnaar gekeken worden in je STARRTS. 
Daarachter zag ik voorbeelden staan wat ik zou kunnen gebruiken, hier heb ik mij ook aan gehouden. 
Vervolgens pakte ik ook de PowerPoint erbij zodat ik niks zou vergeten. Na een tijdje begon het schrijven makkelijker te worden 
omdat ik in mijn blog precies had staan wat ik gedaan had of wat de situatie was. 
Zelf vind ik het idee van dat je dit kwartaal je competenties hoogstwaarschijnlijk toch niet haalt maar beetje vaag.  
Iedereen stopt er enorm veel moeite in en ik zie nu al na vandaag dat ik zeker wel 2 uur eraan heb gezeten om het zo goed mogelijk op papier te krijgen. 
Na deze STARRT ben ik ook gestopt en heb ik het even laten rusten.

  <img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/starrt-4-5.pdf" alt="samenwerken">
