---
title: Teamcoach,planning & inspiratie opdoen  
date: 2017-08-31
---
**<i class="icon-file"></i> Briefing info:**
*De opdrachtgever is de Hogeschool Rotterdam. 
Ze willen dat wij als groep een offline tastbaar en online interactief spel bedenken, 
waarbij de eerstejaarsstudenten elkaar en Rotterdam leren kennen. 
Ze moeten zich op hun gemak voelen met de andere mede studenten en de omgeving Rotterdam.*


Na het krijgen van de briefing werd ik aangewezen als Teamcoach. Ze vonden mij een geschikte kandidaat om dit kwartaal te coachen.
Hoe gaan we de eerste iteratie aanpakken? Ik kreeg de taak om erachter te komen wat de kwaliteiten en vaardigheden waren van mijn team, 
wat voor skills ze al hadden en waar ze goed in zijn. Dat heb ik opgeschreven en doorgegeven aan Ricardio voor de planing. 

Ik ben de belangrijke tekst uit de briefing gaan halen en een debriefing in mijn dummy gaan schrijven. 
Daardoor kon ik beter brainstormen naar een idee voor het spel.

Vervolgens ben ik door de stad gaan lopen om inspiratie op te doen, ik kwam op vele diverse ideeën uit. 
Ik maakte verschillende foto’s van Rotterdam. Maar al snel liep ik vast, omdat ik zelf nog geen kennis heb van game design. 
Met de ideeën die ik verzonnen had hield ik het er even bij. 
 <img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/planing en debriefing.jpg" alt="planning">



