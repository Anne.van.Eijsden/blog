---
title: Hoorcollege prototype,functioneringsgesprek & iteratie 1 afgerond
date: 2017-09-18
---
**Hoorcollege:**
Doormiddel van een goed onderzoek naar het probleem kom je tot de solution. 
Je gaat kijken wat relevant is en vanuit daar ga je een passend concept creëren. 
Om een passend concept te creëren moet je eerst goed de breedte in gaan en vervolgens de diepte. 
Eerst divergeren en daarna convergeren in de dubbel diamant. 
Doordat je deze techniek gebruikt kom je tot originele ideeën en kun je beter 
onderscheid maken tussen dichtbij liggende concepten of ver weg liggende. 
Voor een prototype hoef je nog niet echt in detail te werken, doormiddel van vakjes weet je al snel hoe het werkt. 

**Functioneringsgesprek:**
Het viel mij op als Teamcoach dat de sfeer in de groep niet helemaal lekker zat. 
We hebben op het begin afspraken met elkaar gemaakt en daar zou iedereen naar streven. 
Maar wat mij opviel was dat, wanneer er een mogelijkheid was er iemand naar huis ging. 
Ook waren de spullen niet goed op orde. Zoals bestanden op andere laptop mee en de verkeerde laptop op school. 
Ik vond dat hierover gepraat moest worden. Na het hoorcollege ben ik naar mijn teamgenoten gegaan en gevraagd 
of ze het een goed idee vinden om even rond de tafel te gaan zitten. Iedereen gaf elkaar opbouwende feedback waar je wat aan had. 
Ik had degene waar het niet zo lekker mee ging eerst gevraagd of er iets persoonlijks was en uitgelegd wat 
ik dacht dat er aan de hand was. Diegene brak en waardeerde enorm dat ik haar ogen opende door wat ik zei. 
Ook wat de andere zeiden over elkaar deed goed. Ze bedankte mij later voor het duwtje in de rug voor het gesprek. 
Als ik dit niet voorgesteld had hadden we denk ik veel ruzie gehad. Ik merkte gelijk dat we weer met een hele andere sfeer aan tafel zaten.
Na het functioneringsgesprek had ik alles van het team ingeleverd en was iteratie 1 afgerond.
