---
title: Werkcollege labels
date: 2017-09-27
---
Met het werkcollege ging je te werk met lego vervolgens ging je deze een label geven. 
Sorteren groot naar klein – klein naar groot, vierkant – rond, dik dun. Deze moesten vervolgens elkaar kruisen.
Dit deed je ook met de foto’s. Conclusie: Doordat je gebruik maakt van lego kun je veel breder denken. 
Op klein, vorm, stapelen, ect. Met afbeeldingen ga je al snel kijken naar de emotie en maak je al sneller 
een besluit dat iemand verdrietig is of kwaad. Maar misschien is dat wel helemaal niet zo. 
Door afbeeldingen heb je wel meer houvast maar kun je niet breder ingaan met het onderzoeken. 
Dit omdat je alles veel sneller in hokjes plaatst wat misschien niet het juiste hokje zou kunnen zijn.
 <img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/labels.jpg" alt="spelidee">