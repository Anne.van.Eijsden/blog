---
title: Start Iteratie 3
date: 2017-10-13
---
Laatste fase en leukste fase! Omdat we bijna vakantie hebben en dan minder goed kunnen communiceren wat we nou echt willen, 
heb ik de briefing nog eens goed nagekeken. Wat willen ze zien? En wat moeten we nog maken? 
Op een papiertje schreef ik op wat er gemaakt moest worden en vroeg wie wat wou doen. Dit werd onze taakverdeling voor de vakantie. 
Omdat ik teamcoach ben vond ik het belangrijk dat we nog gezamenlijk naar de feedback van de presentatie keken. 
Hiervan heb ik een notitie van gemaakt wat voor ons de belangrijke puntjes zijn om aandacht aan te besteden en opgestuurd op natschool.

 <img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/feedback.jpg" alt="feedback">

