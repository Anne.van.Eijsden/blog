---
title: Spelregels herschrijven & vormgeven
date: 2017-10-23
---
Bij mij stond er nog één ding op de planning op het gebied van afronden in de vakantie, het spelregel blad voor de teams. 
Ik ben de uitgebreide spelregels gaan samenvatten tot een duidelijke korte uitleg.Aan de groep liet ik een aantal opzetjes zien, daar werd dit ontwerp uitgekozen.
Dit stuurde ik op in de groep en vroeg of iemand voor mij nog extra naar de spellingsfouten wou kijken. 
Deze werden aangepast en ook dit kon ik van mijn lijstje afstrepen.

 <img class="logo-img" src="https://anne.van.eijsden.gitlab.io/blog/img/spelregelkaart.pdf" alt="spelregelkaart">

 